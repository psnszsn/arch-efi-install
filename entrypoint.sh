#!/bin/bash
set -uo pipefail

pacman -Sy --noconfirm --needed dialog reflector || { echo "Error!!!"; exit; }
reflector --verbose --country Romania -l 10 --sort rate --save /etc/pacman.d/mirrorlist
timedatectl set-ntp true


export IHOST=$(dialog --output-fd 1 --no-cancel --inputbox "Enter a name for your computer." 0 0)
export ITZ=$(dialog --output-fd 1 --defaultno --title "Time Zone select" --yesno "Do you want use the default time zone(Europe/Bucharest)?.\n\nPress no for select your own time zone" 10 60 && echo "Europe/Bucharest" || tzselect )
export IDISK=$(dialog --output-fd 1 --menu "Select installtion disk" 0 0 0 $(lsblk -dlnp -o name,size))
export IUSER=$(dialog --output-fd 1 --inputbox "Enter admin username" 0 0 )
export IPASS=$(dialog --output-fd 1 --passwordbox "Enter admin password" 0 0)

DISK=$IDISK

[[ ${DISK: -1} =~ ^[0-9]+$ ]] &&  P="p" || P=""

export EFIPART="${DISK}${P}1"
export SYSPART="${DISK}${P}2"

clear
echo "CANCEL NOW IF YOU DO NOT KNOW WHAT YOU ARE DOING"
sleep 5



sgdisk -o $DISK
sgdisk -n 1::+512M -c 1:"EFI System Partition" -t 1:ef00 $DISK
sgdisk -n 2:: -c 2:"Arch root" $DISK
sgdisk -p $DISK

partprobe

yes | mkfs.vfat -F32 ${EFIPART}
cat <<EOF | cryptsetup -y -v luksFormat --type luks2 $SYSPART
${IPASS}
${IPASS}
EOF

cat <<EOF | cryptsetup open $SYSPART cryptroot
${IPASS}
EOF

yes | mkfs.ext4 /dev/mapper/cryptroot
mount /dev/mapper/cryptroot /mnt
mkdir -p /mnt/boot
mount ${EFIPART} /mnt/boot


wget https://gitlab.com/psnszsn/arch-efi-install/raw/master/scripts/arch.sh
bash arch.sh
rm arch.sh
