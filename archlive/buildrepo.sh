#!/bin/bash
pushd /tmp
mkdir -p /tmp/pkgs
pushd pkgs

curl -L -0 https://aur.archlinux.org/cgit/aur.git/snapshot/yay-bin.tar.gz --output - | tar -xvz
curl -L -0 https://aur.archlinux.org/cgit/aur.git/snapshot/j4-dmenu-desktop.tar.gz --output - | tar -xvz
curl -L -0 https://aur.archlinux.org/cgit/aur.git/snapshot/i3status-rust-bin.tar.gz --output - | tar -xvz

pushd j4-dmenu-desktop && makepkg -f --noconfirm --skippgpcheck; popd
pushd yay-bin && makepkg -f --noconfirm; popd
pushd i3status-rust-bin && makepkg -f --noconfirm; popd
popd

mkdir -p /tmp/repo
repo-add /tmp/repo/custom.db.tar.gz /tmp/pkgs/*/*.pkg.*
cp /tmp/pkgs/*/*.pkg.* /tmp/repo/
popd



