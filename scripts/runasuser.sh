#!/bin/bash

wget https://gitlab.com/psnszsn/arch-efi-install/raw/master/pkg-list.txt
yay -Sy --noconfirm - < pkg-list.txt

git clone --recurse-submodules -j2 https://gitlab.com/psnszsn/vladDotfiles.git
cd vladDotfiles
stow stow

git remote set-url origin git@gitlab.com:psnszsn/vladDotfiles.git
