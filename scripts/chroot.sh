#!/bin/bash

PUUID=$(blkid -s UUID -o value ${SYSPART})
RAM=$(free -m | grep Mem | awk '{print $2}')

# fallocate -l ${RAM}M /swapfile
dd if=/dev/zero of=/swapfile bs=1M count=${RAM} status=progress
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile
printf "/swapfile none swap defaults 0 0\n" >> /etc/fstab
OFFSET=$(filefrag -v /swapfile | sed -n "4p" | awk '{print $4}' | sed 's|[^0-9]||g')

bootctl install
printf "default  arch\ntimeout  4" > /boot/loader/loader.conf

cat<<EOF >> /boot/loader/entries/arch.conf
title   Arch Linux
linux   /vmlinuz-linux
initrd  /initramfs-linux.img
options  rd.luks.name=${PUUID}=cryptroot root=/dev/mapper/cryptroot resume=/dev/mapper/cryptroot resume_offset=${OFFSET} rw
EOF
# options cryptdevice=UUID=${PUUID}:cryptroot root=/dev/mapper/cryptroot resume=/dev/mapper/cryptroot resume_offset=${OFFSET} rw

# refind-install
# cat <<EOF >> /boot/EFI/refind/refind.conf
# scanfor manual,external
# menuentry "Arch Linux" {
#     icon     /EFI/refind/refind-theme-regular/icons/256-96/os_arch.png
#     volume   "Arch Linux"
#     loader   /vmlinuz-linux
#     initrd   /initramfs-linux.img
#     options  "rd.luks.name=${PUUID}=cryptroot root=/dev/mapper/cryptroot resume=/dev/mapper/cryptroot resume_offset=${OFFSET} ro"
#     submenuentry "Boot using fallback initramfs" {
#         initrd /boot/initramfs-linux-fallback.img
#     }
#     submenuentry "Boot to terminal" {
#         add_options "systemd.unit=multi-user.target"
#     }
# }
# EOF


# sudo sh -c "$(curl -fsSL https://raw.githubusercontent.com/bobafetthotmail/refind-theme-regular/master/install.sh)"
echo "KEYMAP=ro" > /etc/vconsole.conf

sed '/^HOOKS/s/^/# /g' /etc/mkinitcpio.conf
echo "HOOKS=(base systemd autodetect modconf block filesystems keyboard fsck sd-encrypt sd-vconsole)" >> /etc/mkinitcpio.conf
mkinitcpio -p linux

ln -sf /usr/share/zoneinfo/$ITZ /etc/localtime
hwclock --systohc

echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
# echo "en_US ISO-8859-1" >> /etc/locale.gen
locale-gen

echo LANG=en_US.UTF-8 >> /etc/locale.conf
#echo LANGUAGE=en_US >> /etc/locale.conf
#echo LC_ALL=C >> /etc/locale.conf


systemctl enable fstrim.timer
systemctl enable systemd-networkd
systemctl enable systemd-resolved



# useradd -mU -G wheel,uucp,video,audio,storage,games,input "$(cat user.installtmp)"
# echo $(cat user.installtmp):$(cat pass.installtmp) | chpasswd
# rm pass.installtmp

# sed -i '/%wheel ALL=(ALL) NOPASSWD: ALL/s/^#//g' /etc/sudoers

# su - $NEWUSER -c "curl -L -0 https://aur.archlinux.org/cgit/aur.git/snapshot/yay-bin.tar.gz --output - | tar -xvz && cd yay-bin && makepkg -si --noconfirm"

# wget -O "/home/$NEWUSER/runasuser.sh" https://gitlab.com/psnszsn/arch-efi-install/raw/master/scripts/runasuser.sh
# sudo -u $NEWUSER bash -c "cd && bash runasuser.sh"
# rm "/home/$NEWUSER/runasuser.sh"

# sed -i '/%wheel ALL=(ALL) NOPASSWD: ALL/s/^/#/g' /etc/sudoers
# sed -i '/%wheel ALL=(ALL) ALL/s/^#//g' /etc/sudoers

# # systemctl enable gdm
# passwd -dl root
