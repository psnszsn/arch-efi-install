#!/bin/bash


pacstrap /mnt base base-devel linux linux-firmware neovim git dialog wget iwd

genfstab -U /mnt >> /mnt/etc/fstab


cat <<EOF > /mnt/etc/hosts
127.0.0.1	localhost
::1	    	localhost
127.0.1.1	$IHOST.localdomain	$IHOST
EOF

echo $IHOST > /mnt/etc/hostname


wget -O /mnt/chroot.sh https://gitlab.com/psnszsn/arch-efi-install/raw/master/scripts/chroot.sh
arch-chroot /mnt bash chroot.sh
rm /mnt/chroot.sh
